1. User has to entry patient detail. Then user is redirected to the patient detail page.
2. In patient detail page there are following sections.
    i. Patient detail
    ii. Followup lists with detail page link
    iii. Activity lists with detail page link
    iv. New followup create option
    v. Last  followup update option
    vi. New activity create option
    vii. All activity update link
    If last followup schedule is still on the way, new schedule is not created. 

3. In activity create page, user can entry doctor, labtest, report, medicines, doctor advice and also assign a new followup schedule..
4. Doctor listing and detail page gives information about doctors, their respective schedule and patients checked up by them.
5. Schedule management is another good feature of this app
6. All the followup patients are listed on the home page according to the date of followup(Nearest at the top), so that user can remind patient with their phone.



Features:
-------------
1. Patient information management
2. Doctor information management
3. Visits and report information management
4. Followup scheduling and management
5. Staff User management
6. Patient Account Infomation management
7. Daywise patient visit statistics
8. Account transaction statistics