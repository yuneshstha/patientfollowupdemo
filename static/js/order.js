$(window).load(function() {
	jQuery('#style-switcher').animate({opacity:1},400); 
});


jQuery(function(){

	
	
	/* Show or hide themes panel
	----------------------------------------------------------*/
	themes_panel_width = jQuery('#style-switcher-menu').outerWidth();
	
	jQuery('#style-switcher').css('right', 0);
	jQuery('#style-switcher').animate({right: -themes_panel_width}, 400);
	
	jQuery('#toggle_button').click(function() {

		var themes_panel = jQuery(this).parent();
		themes_panel.animate({
		  right: parseInt(themes_panel.css('right'),0) == 0 ? -themes_panel_width : 0
		}, 400);
		
		return false;
	});
});