from django_summernote.widgets import SummernoteWidget
from django import forms
from .models import *


class FirstSetupForm(forms.ModelForm):
    class Meta:
        model = Organization
        fields = ['name', 'proprietor', 'expiry_date', 'logo']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Organization Name...'
            }),
            'proprietor': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Proprietor of the Organization...'
            }),
            'expiry_date': forms.DateTimeInput(attrs={
                'class': 'form-control  datetime-picker hidden-sm-down',
                'placeholder': 'Enter Expiry Date...',
                'autocomplete': 'off'
            }),
            'logo': forms.ClearableFileInput(attrs={
                'class': 'form-control',
            }),
        }

    def clean(self):
        if Organization.objects.all().exists():
            raise forms.ValidationError(
                "Organization Already Exists. You can not create New One")
        return Organization.objects.all().first()


class OrderForm(forms.ModelForm):
    class Meta:
        model = SoftwareOrder
        fields = ['name', 'phone', 'email', 'organization', 'address']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Your Name',
                'autocomplete': 'off',
            }),
            'phone': forms.NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'Contact Number',
                'autocomplete': 'off',

            }),
            'email': forms.EmailInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter your mail address',
                'autocomplete': 'off',

            }),
            'organization': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Organization Name',
                'autocomplete': 'off',

            }),
            'address': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Organization Address',
                'autocomplete': 'off',

            }),
        }


class OrganizationForm(forms.ModelForm):
    class Meta:
        model = Organization
        fields = ['name', 'address', 'phone', 'mobile', 'email',
                  'logo', 'slogan', 'image', 'proprietor', 'website']

        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'image': forms.ClearableFileInput(attrs={
                'class': 'form-control',
            }),
            'logo': forms.ClearableFileInput(attrs={
                'class': 'form-control',
            }),
            'address': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'website': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'proprietor': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'mobile': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'phone': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'slogan': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'email': forms.EmailInput(attrs={
                'class': 'form-control',
            }),
        }


class StaffPasswordForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control'
    }))
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control'
    }))

    def clean_confirm_password(self):
        password = self.cleaned_data.get('password')
        confirm_password = self.cleaned_data.get('confirm_password')

        if not confirm_password:
            raise forms.ValidationError("You must confirm your password")
        if password != confirm_password:
            raise forms.ValidationError("Your passwords do not match")

        return confirm_password


class StaffUpdateForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
    }))
    email = forms.CharField(widget=forms.EmailInput(attrs={
        'class': 'form-control'
    }))


class StaffForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'autocomplete': 'off',
    }), )
    email = forms.CharField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'autocomplete': 'off',
    }), )
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'autocomplete': 'off',
    }), )
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'autocomplete': 'off',
    }), )

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if not password2:
            raise forms.ValidationError("You must confirm your password")
        if password1 != password2:
            raise forms.ValidationError("Your passwords do not match")

        return password2


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type': 'text',
        'autocomplete': 'off',
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type': 'password',
        'autocomplete': 'new-password',
    }))


class DoctorForm(forms.ModelForm):
    class Meta:
        model = Doctor
        fields = ['name', 'mobile', 'email', 'image',
                  'speciality', 'education', 'experience', 'details']

        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'image': forms.ClearableFileInput(attrs={
                'class': 'form-control',
            }),
            'address': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'education': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'speciality': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'mobile': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'experience': forms.NumberInput(attrs={
                'class': 'form-control',
            }),
            'email': forms.EmailInput(attrs={
                'class': 'form-control',
            }),
            'details': SummernoteWidget(),
        }


class DoctorScheduleForm(forms.ModelForm):
    class Meta:
        model = DoctorSchedule
        fields = ['arrival', 'departure']

        widgets = {
            'arrival': forms.TextInput(attrs={
                'class': 'form-control timepicker',
                'autocomplete': 'off'
            }),
            'departure': forms.TextInput(attrs={
                'class': 'form-control timepicker',
                'autocomplete': 'off'
            }),
        }


class PatientForm(forms.ModelForm):
    class Meta:
        model = Patient
        fields = ['name',
                  # 'patient_code',
                  'gender', 'age', 'blood_group', 'address',
                  'mobile', 'email', 'doctor']

        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            # 'patient_code': forms.TextInput(attrs={
            #     'class': 'form-control',
            # }),
            'gender': forms.Select(attrs={
                'class': 'form-control select2',
                'data-placeholder': 'Select Gender',
                'data-minimum-results-for-search': 'Infinity',
            }),
            'age': forms.NumberInput(attrs={
                'class': 'form-control',
            }),
            'address': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'mobile': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'email': forms.EmailInput(attrs={
                'class': 'form-control',
            }),
            'doctor': forms.Select(attrs={
                'class': 'form-control select2',
                'data-placeholder': 'Assigned Doctor',
            }),
            'blood_group': forms.Select(attrs={
                'class': 'form-control select2',
                'data-placeholder': 'Blood group',
            })
        }


class PatientReferForm(forms.ModelForm):
    class Meta:
        model = Patient
        fields = ['referred_to']
        widgets = {
            'referred_to': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Referred To',
            })
        }


class ScheduleForm(forms.ModelForm):
    class Meta:
        model = Schedule
        fields = ['patient', 'subject', 'schedule_date']
        widgets = {
            'patient': forms.Select(attrs={
                'class': 'select2',
                'data-placeholder': 'Select Patient',
            }),
            'subject': forms.TextInput(attrs={
                'class': 'form-control bg-white',
                'placeholder': 'Purpose of scheduling',
                'autocomplete': 'off'
            }),
            'schedule_date': forms.DateTimeInput(attrs={
                'class': 'form-control bg-white datetime-picker',
                'placeholder': 'Choose date',
                'autocomplete': 'off'
            })
        }


class LabTestForm(forms.ModelForm):
    class Meta:
        model = LabTest
        fields = ['name', 'charge']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'charge': forms.NumberInput(attrs={
                'class': 'form-control',
            }),
        }


class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = ['patient', 'doctor', 'labtest', 'report', 'medicines',
                  'doctor_advice', 'assign_schedule', 'subject',
                  'next_schedule']
        widgets = {
            'patient': forms.Select(attrs={
                'class': 'form-control select2',
                'data-placeholder': 'Select Patient',
            }),
            'doctor': forms.Select(attrs={
                'class': 'form-control select2',
                'data-placeholder': 'Select Doctor',
            }),
            'labtest': forms.SelectMultiple(attrs={
                'class': 'form-control select2',
                'data-placeholder': 'Select Tests',
            }),
            'report': SummernoteWidget(),
            'medicines': SummernoteWidget(),
            'doctor_advice': SummernoteWidget(),
            'assign_schedule': forms.CheckboxInput(attrs={
                'type': 'checkbox',
                'class': 'checkbox',
                'style': 'cursor: pointer;'
            }),
            'subject': forms.TextInput(attrs={
                'class': 'form-control',
                'disabled': 'true',
            }),
            'next_schedule': forms.DateTimeInput(attrs={
                'class': 'form-control datetime-pickernext',
                'autocomplete': 'off',
                'disabled': 'true',
            })
        }


class TransactionForm(forms.ModelForm):
    class Meta:
        model = Transaction
        fields = ['patient', 'deposited_by', 'payment_method', 'mobile',
                  'service_name', 'transaction_no', 'doctor_charge',
                  'medicine_charge', 'labtest_charge',
                  'misc_charge', 'discount', 'vat', 'net', 'advance',
                  'previous_due', 'due', 'description']

        widgets = {
            'patient': forms.Select(attrs={
                'class': 'form-control select2 d-inline-block text-input',
                'data-placeholder': 'Select Patient...',
                'style': '',
            }),
            'deposited_by': forms.TextInput(attrs={
                'class': 'form-control d-inline-block text-input',
                'placeholder': 'Deposited By...',
                'style': '',
            }),
            'payment_method': forms.Select(attrs={
                'class': 'form-control select2 d-inline-block text-input',
                'style': '',
            }),
            'mobile': forms.TextInput(attrs={
                'class': 'form-control d-inline-block text-input',
                'placeholder': 'Mobile Number...',
                'style': '',
            }),
            'service_name': forms.TextInput(attrs={
                'class': 'form-control d-inline-block text-input',
                'placeholder': 'Service Name',
                'style': '',
            }),
            'doctor_charge': forms.NumberInput(attrs={
                'class': 'form-control input form-input-text-select number-input',
                'aria-describedby': "inputGroup-sizing-lg",
                'placeholder': 'Doctor Charge...',
                'onkeyup': 'findTotal()',
                'onchange': 'findTotal()',
            }),
            'medicine_charge': forms.NumberInput(attrs={
                'class': 'form-control input form-input-text-select number-input',
                'aria-describedby': "inputGroup-sizing-lg",
                'placeholder': 'Medicine Charge...',
                'onkeyup': 'findTotal()',
                'onchange': 'findTotal()',
            }),
            'labtest_charge': forms.NumberInput(attrs={
                'class': 'form-control input form-input-text-select number-input',
                'aria-describedby': "inputGroup-sizing-lg",
                'placeholder': 'Labtest Charge...',
                'onkeyup': 'findTotal()',
                'onchange': 'findTotal()',
            }),
            'misc_charge': forms.NumberInput(attrs={
                'class': 'form-control input form-input-text-select number-input',
                'aria-describedby': "inputGroup-sizing-lg",
                'placeholder': 'MIscellaneous Charge...',
                'onkeyup': 'findTotal()',
                'onchange': 'findTotal()',
            }),
            'discount': forms.NumberInput(attrs={
                'class': 'form-control d-inline-block input form-input-text-select number-input',
                'aria-describedby': "inputGroup-sizing-lg",
                'onkeyup': 'findTotal()',
                'onchange': 'findTotal()',
                'style': 'width:50%;',
            }),
            'vat': forms.NumberInput(attrs={
                'class': 'form-control d-inline-block input form-input-text-select number-input',
                'aria-describedby': "inputGroup-sizing-lg",
                'onkeyup': 'findTotal()',
                'onchange': 'findTotal()',
                'style': 'width:50%;',
            }),
            'net': forms.NumberInput(attrs={
                'class': 'form-control d-inline-block input form-input-text-select number-input',
                'aria-describedby': "inputGroup-sizing-lg",
                'onkeyup': 'findTotal()',
                'onchange': 'findTotal()',
                'style': 'width:50%;',
                'readonly': 'readonly',
            }),
            'advance': forms.NumberInput(attrs={
                'class': 'form-control d-inline-block input form-input-text-select number-input',
                'aria-describedby': "inputGroup-sizing-lg",
                'onkeyup': 'findTotal()',
                'onchange': 'findTotal()',
                'style': 'width:50%;',
            }),
            'previous_due': forms.NumberInput(attrs={
                'class': 'form-control d-inline-block input form-input-text-select number-input',
                'aria-describedby': "inputGroup-sizing-lg",
                'onkeyup': 'findTotal()',
                'onchange': 'findTotal()',
                'style': 'width:50%;',
                'readonly': 'readonly',
            }),
            'due': forms.NumberInput(attrs={
                'class': 'form-control d-inline-block input form-input-text-select number-input',
                'aria-describedby': "inputGroup-sizing-lg",
                'style': 'width:50%;',
                'readonly': 'readonly',
            }),
            'description': forms.Textarea(attrs={
                'class': 'form-control w-100 px-3 py-2',
                'style': 'border:1px solid;',
            }),

        }
