from django.apps import AppConfig


class SfsappConfig(AppConfig):
    name = 'sfsapp'
