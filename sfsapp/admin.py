from django.contrib import admin
from .models import *


admin.site.register([SoftwareOrder, Organization, Doctor, Patient,
                     Schedule, LabTest, Activity, Transaction])
