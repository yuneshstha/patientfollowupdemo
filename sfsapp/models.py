from datetime import datetime, timedelta
import pytz
from django.core.exceptions import ValidationError
from django.db.models.signals import pre_save
from django.contrib.auth.models import User
from .utils import unique_slug_generator
from django.db import models


class SoftwareOrder(models.Model):
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=10)
    email = models.EmailField(null=True, blank=True)
    address = models.CharField(max_length=200, null=True, blank=True)
    organization = models.CharField(max_length=200, null=True, blank=True)


class Organization(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200, null=True, blank=True)
    phone = models.CharField(max_length=10, null=True, blank=True)
    mobile = models.CharField(max_length=50, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    website = models.URLField(null=True, blank=True)
    logo = models.ImageField(upload_to="organization", null=True, blank=True)
    image = models.ImageField(upload_to='organization', null=True, blank=True)
    slogan = models.CharField(max_length=400, null=True, blank=True)
    proprietor = models.CharField(max_length=100)
    registered_date = models.DateTimeField(auto_now=True)
    expiry_date = models.DateTimeField()

    def __str__(self):
        return self.name

    # def save(self, *args, **kwargs):
    #     if Organization.objects.exists() and not self.pk:
    #         raise ValidationError(
    #             'Organiztion already added')
    #     return super(Organization, self).save(*args, **kwargs)


class Doctor(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(unique=True, null=True, blank=True)
    mobile = models.CharField(max_length=50)
    email = models.EmailField(null=True, blank=True)
    image = models.ImageField(upload_to='doctors', null=True, blank=True)
    speciality = models.CharField(max_length=200)
    education = models.CharField(max_length=200)
    experience = models.FloatField(null=True, blank=True)
    details = models.TextField(null=True, blank=True)
    added_on = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(
        User, on_delete=models.SET_NULL,
        related_name="dcb", null=True, blank=True)
    updated_by = models.ForeignKey(
        User, on_delete=models.SET_NULL,
        related_name="dub", null=True, blank=True)

    def __str__(self):
        return self.name + ' ( ' + self.speciality + ' )'

    @property
    def title(self):
        return self.name


DAY = (
    ('sunday', 'sunday'),
    ('monday', 'monday'),
    ('tuesday', 'tuesday'),
    ('wednesday', 'wednesday'),
    ('thursday', 'thursday'),
    ('friday', 'friday'),
    ('saturday', 'saturday'),
)


class DoctorSchedule(models.Model):
    doctor = models.ForeignKey(
        Doctor, on_delete=models.CASCADE, related_name='doctorschedules')
    day = models.CharField(max_length=20, choices=DAY)
    arrival = models.CharField(max_length=30)
    departure = models.CharField(max_length=30)
    created_by = models.ForeignKey(
        User, on_delete=models.SET_NULL,
        related_name="dscb", null=True, blank=True)
    updated_by = models.ForeignKey(
        User, on_delete=models.SET_NULL,
        related_name="scub", null=True, blank=True)

    def __str__(self):
        return self.doctor.name


GENDER = (
    ('Male', 'Male'),
    ('Female', 'Female'),
    ('Others', 'Others'),
)

BLOOD_GROUP = (
    ('A+', 'A+'),
    ('B+', 'B+'),
    ('AB+', 'AB+'),
    ('O+', 'O+'),
    ('A-', 'A-'),
    ('B-', 'B-'),
    ('AB-', 'AB-'),
    ('O-', 'O-'),
)


class Patient(models.Model):
    name = models.CharField(max_length=200)
    patient_code = models.CharField(
        max_length=30, unique=True, null=True, blank=True)
    slug = models.SlugField(unique=True, null=True, blank=True)
    gender = models.CharField(max_length=10, choices=GENDER)
    age = models.PositiveIntegerField()
    blood_group = models.CharField(
        max_length=5, choices=BLOOD_GROUP, null=True, blank=True)
    admitted_on = models.DateTimeField(auto_now=True)
    address = models.CharField(max_length=200)
    mobile = models.CharField(max_length=50)
    email = models.EmailField(null=True, blank=True)
    doctor = models.ForeignKey(
        Doctor, on_delete=models.SET_NULL,
        related_name='docpatients', null=True)
    created_by = models.ForeignKey(
        User, on_delete=models.SET_NULL, related_name="pcb",

        null=True, blank=True)
    updated_by = models.ForeignKey(
        User, on_delete=models.SET_NULL, related_name="pub",
        null=True, blank=True)

    total_amount = models.FloatField(default=0)
    paid_amount = models.FloatField(default=0)
    due_amount = models.FloatField(default=0)
    is_referred = models.BooleanField(default=False)
    referred_to = models.CharField(max_length=100, null=True, blank=True)
    referred_on = models.DateTimeField(
        auto_now_add=True, null=True, blank=True)

    @property
    def title(self):
        return self.name

    def __str__(self):
        return self.name + " (Patient Code: " + self.patient_code + ")"

    def save(self):
        self.patient_code = str(self.id)
        return super().save()


class Schedule(models.Model):
    patient = models.ForeignKey(
        Patient, on_delete=models.CASCADE,
        null=True, blank=True, related_name='schedules')
    slug = models.SlugField(unique=True, null=True, blank=True)
    scheduled_on = models.DateTimeField(auto_now=True)
    subject = models.CharField(max_length=200)
    schedule_date = models.DateTimeField()
    contacted_date = models.DateTimeField(null=True, blank=True)
    created_by = models.ForeignKey(
        User, on_delete=models.SET_NULL,
        related_name="scb", null=True, blank=True)
    updated_by = models.ForeignKey(
        User, on_delete=models.SET_NULL,
        related_name="sub", null=True, blank=True)
    contacted = models.BooleanField(default=False)
    visited = models.BooleanField(default=False)

    class Meta:
        ordering = ['-schedule_date']

    @property
    def title(self):
        return self.patient.name + "-" + self.subject

    # @property
    # def is_lessthan_3days(self):
    #     return self.schedule_date < datetime.now() + timedelta(days=3)

    def __str__(self):
        return self.patient.name


class LabTest(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(unique=True, null=True, blank=True)
    charge = models.FloatField(default=0)
    created_by = models.ForeignKey(
        User, on_delete=models.SET_NULL,
        related_name="lcb", null=True, blank=True)
    updated_by = models.ForeignKey(
        User, on_delete=models.SET_NULL,
        related_name="lub", null=True, blank=True)

    @property
    def title(self):
        return self.name

    def __str__(self):
        return self.name


# class LabTestReport(models.Model):
#     patient = models.ForeignKey(
#         Patient, on_delete=models.CASCADE, related_name='labtestreports')
#     test = models.ForeignKey(LabTest, on_delete=models.SET_NULL,
#                              null=True, related_name='labtestreports')
#     report = models.ImageField(
#         upload_to='labtestreports', null=True, blank=True)
    


class Activity(models.Model):
    patient = models.ForeignKey(
        Patient, on_delete=models.CASCADE, related_name='activities')
    slug = models.SlugField(unique=True, null=True, blank=True)
    doctor = models.ForeignKey(Doctor, null=True,
                               blank=True, on_delete=models.SET_NULL,
                               related_name='activities')
    activity_on = models.DateTimeField(auto_now=True)
    labtest = models.ManyToManyField(
        LabTest, related_name='activities', blank=True)
    report = models.TextField(null=True, blank=True)
    medicines = models.TextField(null=True, blank=True)
    doctor_advice = models.TextField(null=True, blank=True)
    assign_schedule = models.BooleanField(default=False)
    subject = models.CharField(max_length=200, null=True, blank=True)
    next_schedule = models.DateTimeField(null=True, blank=True)
    created_by = models.ForeignKey(
        User, on_delete=models.SET_NULL,
        related_name="acb", null=True, blank=True)
    updated_by = models.ForeignKey(
        User, on_delete=models.SET_NULL,
        related_name="aub", null=True, blank=True)

    @property
    def title(self):
        return self.patient.name + "-" + "-" + "activity"

    def __str__(self):
        return self.patient.name


PAYMENT_METHOD = (
    ('cash', 'cash'),
    ('check', 'check'),
)


class Transaction(models.Model):
    patient = models.ForeignKey(
        Patient, on_delete=models.SET_NULL, related_name='transactions', null=True)
    deposited_by = models.CharField(max_length=200)
    mobile = models.CharField(max_length=30)
    date = models.DateTimeField(auto_now=True)
    transaction_no = models.PositiveIntegerField(null=True, blank=True)
    service_name = models.CharField(max_length=200)
    doctor_charge = models.FloatField(default=0)
    medicine_charge = models.FloatField(default=0)
    labtest_charge = models.FloatField(default=0)
    misc_charge = models.FloatField(default=0)
    discount = models.FloatField(default=0)
    vat = models.FloatField(default=0)
    net = models.FloatField(default=0)
    advance = models.FloatField(default=0)
    due = models.FloatField(default=0)
    previous_due = models.FloatField(default=0)
    payment_method = models.CharField(
        max_length=20, choices=PAYMENT_METHOD, default='transaction')
    received_by = models.ForeignKey(
        User, on_delete=models.SET_NULL,
        related_name="transactions", null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.patient.name + "(" + str(self.date) + ")" + " by " + self.deposited_by


def all_pre_save(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(all_pre_save, sender=Doctor)
pre_save.connect(all_pre_save, sender=Patient)
pre_save.connect(all_pre_save, sender=Schedule)
pre_save.connect(all_pre_save, sender=Activity)
pre_save.connect(all_pre_save, sender=LabTest)
