# Generated by Django 2.1.1 on 2018-10-19 09:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sfsapp', '0018_patient_referred_to'),
    ]

    operations = [
        migrations.AddField(
            model_name='patient',
            name='referred_on',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
