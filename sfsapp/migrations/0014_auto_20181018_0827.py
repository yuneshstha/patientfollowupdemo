# Generated by Django 2.1.1 on 2018-10-18 02:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sfsapp', '0013_auto_20181008_2154'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='invoice',
            options={'ordering': ['-id']},
        ),
        migrations.AddField(
            model_name='invoice',
            name='payment_method',
            field=models.CharField(choices=[('check', 'check'), ('cash', 'cash')], default='cash', max_length=20),
            preserve_default=False,
        ),
    ]
