# Generated by Django 2.1.1 on 2018-10-09 07:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sfsapp', '0012_invoice_due'),
    ]

    operations = [
        migrations.AddField(
            model_name='patient',
            name='blood_group',
            field=models.CharField(blank=True, choices=[('A+', 'A+'), ('B+', 'B+'), ('AB+', 'AB+'), ('O+', 'O+'), ('A-', 'A-'), ('B-', 'B-'), ('AB-', 'AB-'), ('O-', 'O-')], max_length=5, null=True),
        ),
    ]
