from django.urls import path
from .views import *


app_name = 'sfsapp'
urlpatterns = [
    path('firstsetup/', FirstSetupView.as_view(), name='firstsetup'),
    path('ordernow/', OrderNowView.as_view(), name='ordernow'),

    path('organization/', OrganizationDetailView.as_view(),
         name='organizationdetail'),
    path('organization/<int:pk>/update/', OrganizationUpdateView.as_view(),
         name='organizationupdate'),

    path('staff-registration/', StaffRegistrationView.as_view(),
         name='staffregistration'),
    path('staff/list/', StaffListView.as_view(), name='stafflist'),
    path('staff/<username>/update/', StaffUpdateView.as_view(),
         name='staffupdate'),
    path('staff/<username>/password/change/',
         StaffPasswordChangeView.as_view(), name='staffpasswordchange'),

    path('', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),

    path('home/', HomeView.as_view(), name='home'),
    path('patientnotfound/', PatientNotFoundView.as_view(), name='patientnotfound'),
    path('search/', PatientSearchView.as_view(), name='patientsearch'),
    path('schedule/contact-status/', ChangeContactStatusView.as_view(),
         name='changecontactstatus'),

    path('doctor/list/', DoctorListView.as_view(), name='doctorlist'),
    path('doctor/<slug:slug>/detail/',
         DoctorDetailView.as_view(), name='doctordetail'),
    path('doctor/add/', DoctorCreateView.as_view(), name='doctorcreate'),
    path('doctor/<slug:slug>/update/',
         DoctorUpdateView.as_view(), name='doctorupdate'),
    path('doctor/<slug:slug>/delete/',
         DoctorDeleteView.as_view(), name='doctordelete'),

    path('doctors/schedules/all/', DoctorScheduleListView.as_view(),
         name='doctorschedulelist'),

    path('doctor/<slug:slug>/schedule/<day>/create/',
         DoctorScheduleCreateView.as_view(), name='doctorschedulecreate'),
    path('doctor/<slug:slug>/schedule/<int:pk>/<day>/update/',
         DoctorScheduleUpdateView.as_view(), name='doctorscheduleupdate'),
    path('doctor/<slug:slug>/schedule/<int:pk>/<day>/delete/',
         DoctorScheduleDeleteView.as_view(), name='doctorscheduledelete'),

    path('patient/list/', PatientListView.as_view(), name='patientlist'),
    path('patient/<slug:slug>/detail/',
         PatientDetailView.as_view(), name='patientdetail'),
    path('patient/create/', PatientCreateView.as_view(), name='patientcreate'),
    path('patient/<slug:slug>/update/',
         PatientUpdateView.as_view(), name='patientupdate'),
    path('patient/<slug:slug>/delete/',
         PatientDeleteView.as_view(), name='patientdelete'),

    path('patient/<slug:slug>/refer/', PatientReferView.as_view(),
         name='patientrefer'),


    # Patients Contacted List
    path('get-patients/list/', GetPatientListView.as_view(),
         name='getpatientlist'),


    path('schedule/list/', ScheduleListView.as_view(), name='schedulelist'),
    path('schedule/<slug:slug>/detail/',
         ScheduleDetailView.as_view(), name='scheduledetail'),
    path('schedule/create/', ScheduleCreateView.as_view(),
         name='schedulecreate'),
    path('schedule/<slug:slug>/update/',
         ScheduleUpdateView.as_view(), name='scheduleupdate'),
    path('schedule/<slug:slug>/delete/',
         ScheduleDeleteView.as_view(), name='scheduledelete'),


    path('labtest/list/', LabTestListView.as_view(), name='labtestlist'),
    # path('labtest/<slug:slug>/detail/',
    #      LabTestDetailView.as_view(), name='labtestdetail'),
    path('labtest/create/', LabTestCreateView.as_view(),
         name='labtestcreate'),
    path('labtest/<slug:slug>/update/',
         LabTestUpdateView.as_view(), name='labtestupdate'),
    path('labtest/<slug:slug>/delete/',
         LabTestDeleteView.as_view(), name='labtestdelete'),


    path('activities/list/',
         ActivityListView.as_view(), name='activitylist'),
    path('activities/<slug:slug>/detail/',
         ActivityDetailView.as_view(), name='activitydetail'),
    path('activities/<slug:slug>/update/',
         ActivityUpdateView.as_view(), name='activityupdate'),
    path('activities/<slug:slug>/update/<notlast>/',
         ActivityUpdateView.as_view(), name='activityupdate'),
    path('activities/create/',
         ActivityCreateView.as_view(), name='activitycreate'),
    path('activities/create/<patient_slug>/<id>/',
         ActivityCreateView.as_view(), name='activitycreate'),
    path('activities/<slug:slug>/delete/',
         ActivityDeleteView.as_view(), name='activitydelete'),


    # Transactions
    path('transaction/list/', TransactionListView.as_view(), name='transactionlist'),
    path('transaction/create/', TransactionCreateView.as_view(),
         name='transactioncreate'),
    path('<slug:slug>/transaction/create/',
         TransactionCreateView.as_view(), name='patienttransactioncreate'),
    path('getpreviousdue/',
         GetPreviousDueView.as_view(), name='getpreviousdue'),
    path('transaction/<int:pk>/detail/',
         TransactionDetailView.as_view(), name='transactiondetail'),
    path('<slug:slug>/transaction/list/',
         PatientTransactionListView.as_view(), name='patienttransactionlist'),

    #Account Summary
    path('accountsummary/', AccountSummaryView.as_view(), name='accountsummary'),

]
