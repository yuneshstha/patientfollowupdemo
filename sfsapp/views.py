from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse, reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.views.generic import *
from django.utils import timezone, dateparse
from .models import *
from .forms import *
from django.conf import settings
from django.core.mail import send_mail, EmailMultiAlternatives


class ExpiryMixin(object):
    def dispatch(self, request, *args, **kwargs):
        try:
            organization = Organization.objects.all().first()
            registered_date = organization.registered_date
            expiry_date = organization.expiry_date
            if expiry_date > timezone.now():
                # print("Time is remaining")
                pass
            else:
                # print("Software Expired")
                return render(request, "login.html",
                              {'form': LoginForm, 'message': "Expired"})
        except Exception as e:
            return render(request, 'firstsetup.html', {'form': FirstSetupForm})
        return super().dispatch(request, *args, **kwargs)


class AdminRequiredMixin(ExpiryMixin, LoginRequiredMixin, object):
    login_url = reverse_lazy('sfsapp:login')

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser is False:
            return render(request, 'permissiondenied.html')
        else:
            return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['organization'] = organization
        context['ordernowform'] = OrderForm

        return context


class OrganizationDetailView(ExpiryMixin, LoginRequiredMixin, TemplateView):
    template_name = 'organizationdetail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['organization'] = organization
        context['ordernowform'] = OrderForm

        return context


class FirstSetupView(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'firstsetup.html'
    form_class = FirstSetupForm
    success_url = reverse_lazy('sfsapp:home')


class OrderNowView(SuccessMessageMixin, CreateView):
    template_name = 'home.html'
    form_class = OrderForm
    success_message = 'ThankYou for ordering this app. We will contact you soon.'
    success_url = reverse_lazy('sfsapp:home')

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.save()
        subject = 'Software Order'
        handler_email = 'sarobartech@gmail.com'
        from_email = settings.EMAIL_HOST_USER
        send_mail(subject, "Thankyou for ordering Patient Followup System From Sarobar Technology. We will contact you soon for confirmation.", from_email,
                  [form.instance.email], fail_silently=True)
        send_mail(subject,
                  "Details: " + ",\n\t" + "Organization Name:" +
                  form.instance.organization + "\n\t" + "Address: " +
                  form.instance.address + "\n\t Contact:" +
                  form.instance.phone + "\n\tEmail Address:" +
                  form.instance.email + "\n\t Contact Person:" +
                  form.instance.name +
                  "\n\n\t You can also check your messages in Patient Followup Admin Panel", from_email,
                  [handler_email], fail_silently=True)
        return super().form_valid(form)


class OrganizationUpdateView(AdminRequiredMixin, UpdateView):
    template_name = 'organizationupdate.html'
    model = Organization
    form_class = OrganizationForm
    success_url = reverse_lazy('sfsapp:home')
    success_message = "Organization updated successfully."


class StaffRegistrationView(SuccessMessageMixin, AdminRequiredMixin, FormView):
    template_name = 'staffregistration.html'
    form_class = StaffForm
    success_url = reverse_lazy('sfsapp:stafflist')
    success_message = "New staff registered successfully."

    def form_valid(self, form):
        user = User.objects.create_user(
            form.cleaned_data['username'],
            form.cleaned_data['email'],
            form.cleaned_data['password1']
        )
        user.is_staff = True
        user.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['organization'] = organization
        return context


class StaffListView(AdminRequiredMixin, ListView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'stafflist.html'
    queryset = User.objects.filter(is_superuser=False)
    context_object_name = 'stafflist'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['organization'] = organization
        return context


class StaffUpdateView(SuccessMessageMixin, AdminRequiredMixin, FormView):
    template_name = 'staffupdate.html'
    model = User
    form_class = StaffUpdateForm
    success_url = reverse_lazy('sfsapp:stafflist')
    success_message = 'Staff information updated successfully.'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['organization'] = organization
        self.staff = User.objects.get(username=self.kwargs['username'])
        context['staff'] = self.staff
        return context

    def form_valid(self, form):
        username = form.cleaned_data['username']
        email = form.cleaned_data['email']
        staff = User.objects.get(username=self.kwargs['username'])
        staff.username = username
        staff.email = email
        staff.save()
        return super().form_valid(form)


class StaffPasswordChangeView(SuccessMessageMixin, AdminRequiredMixin, FormView):
    template_name = 'staffpasswordchange.html'
    form_class = StaffPasswordForm
    success_url = reverse_lazy('sfsapp:stafflist')
    success_message = " Staff's password changed successfully"

    def form_valid(self, form):
        staff = User.objects.get(username=self.kwargs['username'])
        password = form.cleaned_data['confirm_password']
        staff.set_password(password)
        staff.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['organization'] = organization
        context['username'] = self.kwargs['username']
        return context


class LogoutView(LoginRequiredMixin, View):
    login_url = reverse_lazy('sfsapp:login')

    def get(self, request):
        logout(request)
        return redirect('sfsapp:login')


class LoginView(SuccessMessageMixin, FormView):
    template_name = 'login.html'
    form_class = LoginForm
    success_url = reverse_lazy("sfsapp:home")
    success_message = "Logged in successfully"

    # def get(self, request):
    #     if request.user.is_authenticated:
    #         return render(self.request, 'home.html')
    #     return super().get(request)

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username,
                            password=password)
        if user is not None:
            login(self.request, user)
        else:
            return render(self.request, self.template_name, {
                'form': form,
                'errors': "Please correct username or password"})
        return super().form_valid(form)

    def get_success_url(self, *args, **kwargs):
        self.next_url = self.request.POST.get('next')
        if self.next_url is not None:
            return self.next_url
        else:
            return self.success_url

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            organization = Organization.objects.all().first()
            registered_date = organization.registered_date
            expiry_date = organization.expiry_date
            if expiry_date > timezone.now():
                context['organization'] = organization
        except Exception as e:
            pass
        return context


class ChangeContactStatusView(View):
    def get(self, request):
        schedule_id = request.GET.get('id')
        clicked = request.GET.get('clicked')
        schedule = Schedule.objects.get(id=schedule_id)
        if clicked == "clicked":
            schedule.contacted = True
            schedule.contacted_date = timezone.now()
            schedule.updated_by = request.user
            schedule.save()
        followuplist = Schedule.objects.filter(
            schedule_date__gt=timezone.now()).order_by('schedule_date')

        return render(request, 'ajaxcontactstatus.html',
                      {'followuplist': followuplist, 'schedule': schedule})


class HomeView(ExpiryMixin, LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['organization'] = organization
        context['notcontactedpatientlist'] = Schedule.objects.filter(
            schedule_date__gt=timezone.now(), contacted=False).order_by('schedule_date')
        context['contactedpatientlist'] = Schedule.objects.filter(
            schedule_date__gt=timezone.now(), contacted=True, visited=False).order_by('schedule_date')
        context['notvisitedpatientlist'] = Schedule.objects.filter(
            schedule_date__lt=timezone.now(), visited=False).order_by('schedule_date')
        context['scheduleupdateform'] = ScheduleForm
        context['totalpatients'] = Patient.objects.count()
        context['totaldoctors'] = Doctor.objects.count()
        context['totalpatientvisits'] = Activity.objects.count()
        context['ordernowform'] = OrderForm

        return context


class DoctorListView(ExpiryMixin, LoginRequiredMixin, ListView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'doctorlist.html'
    queryset = Doctor.objects.all().order_by('-id')
    context_object_name = 'doctorlist'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['organization'] = organization
        context['ordernowform'] = OrderForm

        return context


class DoctorDetailView(ExpiryMixin, LoginRequiredMixin, DetailView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'doctordetail.html'
    model = Doctor
    context_object_name = 'doctordetail'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['organization'] = organization
        context['ordernowform'] = OrderForm

        context['sundayschedules'] = DoctorSchedule.objects.filter(
            doctor__slug=self.kwargs['slug'], day="sunday")
        context['mondayschedules'] = DoctorSchedule.objects.filter(
            doctor__slug=self.kwargs['slug'], day="monday")
        context['tuesdayschedules'] = DoctorSchedule.objects.filter(
            doctor__slug=self.kwargs['slug'], day="tuesday")
        context['wednesdayschedules'] = DoctorSchedule.objects.filter(
            doctor__slug=self.kwargs['slug'], day="wednesday")
        context['thursdayschedules'] = DoctorSchedule.objects.filter(
            doctor__slug=self.kwargs['slug'], day="thursday")
        context['fridayschedules'] = DoctorSchedule.objects.filter(
            doctor__slug=self.kwargs['slug'], day="friday")
        context['saturdayschedules'] = DoctorSchedule.objects.filter(
            doctor__slug=self.kwargs['slug'], day="saturday")
        return context


class DoctorCreateView(ExpiryMixin, SuccessMessageMixin, LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'doctorcreate.html'
    form_class = DoctorForm
    success_url = reverse_lazy('sfsapp:doctorlist')
    success_message = "New doctor created successfully."

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        self.doctor_slug = form.save().slug
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['organization'] = organization
        context['ordernowform'] = OrderForm

        return context

    def get_success_url(self, **kwargs):
        return reverse('sfsapp:doctordetail',
                       kwargs={'slug': self.doctor_slug})


class DoctorUpdateView(ExpiryMixin, SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'doctorcreate.html'
    model = Doctor
    form_class = DoctorForm
    success_message = "Doctor information updated successfully."

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('sfsapp:doctordetail',
                       kwargs={'slug': self.kwargs['slug']})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['organization'] = organization
        context['ordernowform'] = OrderForm

        return context


class DoctorDeleteView(SuccessMessageMixin, AdminRequiredMixin, DeleteView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'doctordelete.html'
    model = Doctor
    success_url = reverse_lazy('sfsapp:doctorlist')
    success_message = "Doctor deleted successfully."


class DoctorScheduleListView(ExpiryMixin, LoginRequiredMixin, ListView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'doctorschedulelist.html'
    queryset = Doctor.objects.all().order_by('-id')
    context_object_name = 'doctors'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['organization'] = organization
        context['ordernowform'] = OrderForm

        return context


class DoctorScheduleCreateView(ExpiryMixin, SuccessMessageMixin, LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'doctorschedulecreate.html'
    form_class = DoctorScheduleForm
    success_url = reverse_lazy('sfsapp:home')
    success_message = "Schedule created successfully."

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.doctor = Doctor.objects.get(slug=self.kwargs['slug'])
        form.instance.day = self.kwargs['day']
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('sfsapp:doctordetail',
                       kwargs={'slug': self.kwargs['slug']})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['organization'] = organization
        context['ordernowform'] = OrderForm

        return context


class DoctorScheduleUpdateView(ExpiryMixin, SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'doctorschedulecreate.html'
    model = DoctorSchedule
    form_class = DoctorScheduleForm
    success_url = reverse_lazy('sfsapp:home')
    success_message = "Schedule updatedt successfully."

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('sfsapp:doctordetail',
                       kwargs={'slug': self.kwargs['slug']})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['organization'] = organization
        context['ordernowform'] = OrderForm
        return context


class DoctorScheduleDeleteView(SuccessMessageMixin, AdminRequiredMixin, DeleteView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'doctorscheduledelete.html'
    model = DoctorSchedule
    success_message = "Schedule deleted successfully."

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['slug'] = self.kwargs['slug']
        context['day'] = self.kwargs['day']
        return context

    def get_success_url(self):
        return reverse('sfsapp:doctordetail',
                       kwargs={'slug': self.kwargs['slug']})


class PatientListView(ExpiryMixin, LoginRequiredMixin, ListView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'patientlist.html'
    queryset = Patient.objects.all().order_by('-id')
    context_object_name = 'patientlist'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        return context


class PatientDetailView(ExpiryMixin, LoginRequiredMixin, DetailView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'patientdetail.html'
    queryset = Patient.objects.all()
    context_object_name = 'patientdetail'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        patient = Patient.objects.get(slug=self.kwargs['slug'])
        context['schedulecreateform'] = ScheduleForm
        if Schedule.objects.filter(patient=patient, visited=False).exists():
            nextfollowup = Schedule.objects.get(
                patient=patient, visited=False)
            context['nextfollowup'] = nextfollowup
            context['scheduleupdateform'] = ScheduleForm(instance=nextfollowup)
        context['followuplist'] = Schedule.objects.filter(
            patient=patient).order_by('-id')
        context['activitylist'] = Activity.objects.filter(
            patient=patient).order_by('-id')

        return context


class PatientSearchView(ExpiryMixin, LoginRequiredMixin, RedirectView):
    login_url = reverse_lazy('sfsapp:login')
    permanent = False
    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        p_code = self.request.GET.get('search')
        try:
            patient = Patient.objects.get(patient_code=p_code)
            slug = patient.slug
            return reverse('sfsapp:patientdetail', kwargs={'slug': slug})
        except:
            return reverse('sfsapp:patientnotfound')
        return super().get_redirect_url(*args, **kwargs)


class PatientNotFoundView(ExpiryMixin, LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'patientnotfound.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        return context


class PatientCreateView(ExpiryMixin, SuccessMessageMixin, LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'patientcreate.html'
    form_class = PatientForm
    success_url = reverse_lazy('sfsapp:patientlist')
    success_message = "Patient added successfully."

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        self.slug = form.save().slug
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('sfsapp:patientdetail', kwargs={'slug': self.slug})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        return context


class PatientUpdateView(ExpiryMixin, SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'patientcreate.html'
    model = Patient
    form_class = PatientForm
    success_url = reverse_lazy('sfsapp:patientlist')
    success_message = "Patient information updated successfully."

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        self.slug = form.save().slug
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('sfsapp:patientdetail', kwargs={'slug': self.slug})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        return context


class PatientDeleteView(SuccessMessageMixin, AdminRequiredMixin, DeleteView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'patientdelete.html'
    model = Patient
    success_url = reverse_lazy('sfsapp:patientlist')
    success_message = "Patient deleted successfully."


class PatientReferView(SuccessMessageMixin, ExpiryMixin, LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'patientrefer.html'
    form_class = PatientReferForm
    model = Patient
    success_message = "Patient Status Changed to Referred Patient"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.patient = Patient.objects.get(slug=self.kwargs['slug'])
        context['patientdetail'] = self.patient
        return context

    def form_valid(self, form):
        form.instance.is_referred = True

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('sfsapp:patientdetail', kwargs={'slug': self.kwargs['slug']})


from datetime import datetime


class GetPatientListView(ExpiryMixin, LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'getpatientlist.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        # if self.request.method == 'GET':
        from_date = self.request.GET.get('from_date')
        to_date = self.request.GET.get('to_date')
        if from_date and to_date:
            if datetime.strptime(from_date, "%Y-%m-%d").date() <= datetime.strptime(to_date, "%Y-%m-%d").date():
                allpatients = set()
                allvisits = Activity.objects.select_related('patient').filter(
                    activity_on__gte=from_date, activity_on__lte=to_date)
                for visit in allvisits:
                    allpatients.add(visit.patient)
                context['from_date'] = from_date
                context['to_date'] = to_date
                context['allpatients'] = allpatients
                context['allvisits'] = allvisits
            else:
                context['error'] = "Please enter the valid date"
        return context


class ScheduleListView(ExpiryMixin, LoginRequiredMixin, ListView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'schedulelist.html'
    queryset = Schedule.objects.all().order_by('-id')
    context_object_name = 'schedulelist'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        return context


class ScheduleDetailView(ExpiryMixin, LoginRequiredMixin, DetailView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'scheduledetail.html'
    queryset = Schedule.objects.all()
    context_object_name = 'scheduledetail'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        return context


class ScheduleCreateView(ExpiryMixin, SuccessMessageMixin, LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'schedulecreate.html'
    form_class = ScheduleForm
    success_url = reverse_lazy('sfsapp:schedulelist')
    success_message = "Followup schedule created successfully."

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        self.slug = form.save().patient.slug
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('sfsapp:patientdetail', kwargs={'slug': self.slug})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        return context


class ScheduleUpdateView(ExpiryMixin, SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'schedulecreate.html'
    model = Schedule
    form_class = ScheduleForm
    success_url = reverse_lazy('sfsapp:schedulelist')
    success_message = "Followup schedule updated successfully."

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        form.instance.contacted = False
        self.slug = form.save().patient.slug
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('sfsapp:patientdetail', kwargs={'slug': self.slug})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        return context


class ScheduleDeleteView(ExpiryMixin, SuccessMessageMixin, LoginRequiredMixin, DeleteView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'scheduledelete.html'
    model = Schedule
    success_url = reverse_lazy('sfsapp:home')
    success_message = "Followup schedule updated successfully."


class LabTestListView(ExpiryMixin, LoginRequiredMixin, ListView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'labtestlist.html'
    queryset = LabTest.objects.all().order_by('-id')
    context_object_name = 'labtestlist'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        return context


class LabTestCreateView(ExpiryMixin, SuccessMessageMixin, LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'labtestcreate.html'
    form_class = LabTestForm
    success_url = reverse_lazy('sfsapp:labtestlist')
    success_message = "New lab test added successfully."

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        return context


class LabTestUpdateView(ExpiryMixin, SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'labtestcreate.html'
    model = LabTest
    form_class = LabTestForm
    success_url = reverse_lazy('sfsapp:labtestlist')
    success_message = "Lab test updated successfully."

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        return context


class LabTestDeleteView(SuccessMessageMixin, AdminRequiredMixin, DeleteView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'labtestdelete.html'
    model = LabTest
    success_url = reverse_lazy('sfsapp:labtestlist')
    success_message = "Lab test deleted successfully."


class ActivityListView(ExpiryMixin, LoginRequiredMixin, ListView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'activitylist.html'
    queryset = Activity.objects.all().order_by('-id')
    context_object_name = 'activitylist'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        return context


class ActivityDetailView(ExpiryMixin, LoginRequiredMixin, DetailView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'activitydetail.html'
    queryset = Activity.objects.all()
    context_object_name = 'activitydetail'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        return context


class ActivityCreateView(ExpiryMixin, SuccessMessageMixin, LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'activitycreate.html'
    form_class = ActivityForm
    success_url = reverse_lazy('sfsapp:home')
    success_message = "New Visit (Activity) created successfully."

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        try:
            self.patient = Patient.objects.get(
                slug=self.kwargs['patient_slug'])
            form.instance.patient = self.patient
        except Exception as e:
            # print(e)
            self.patient = form.cleaned_data['patient']
            form.instance.patient = self.patient
        if form.cleaned_data['doctor']:
            # print("from cleaned data")
            form.instance.doctor = form.cleaned_data['doctor']
        else:
            # print("from patient")
            form.instance.doctor = self.patient.doctor
        # last followup
        followup_exists = Schedule.objects.filter(
            patient=self.patient,
            visited=False).exists()
        assign_schedule_clicked = form.cleaned_data['assign_schedule']
        if followup_exists is False and assign_schedule_clicked:
            subject = form.cleaned_data['subject']
            date = form.cleaned_data['next_schedule']
            Schedule.objects.create(
                patient=self.patient, subject=subject, schedule_date=date)
        elif followup_exists:
            lastfollowup = Schedule.objects.get(
                patient=self.patient, visited=False)
            if assign_schedule_clicked:
                lastfollowup.schedule_date = timezone.now()
                lastfollowup.visited = True
                lastfollowup.save()
                subj = form.cleaned_data['subject']
                schd_date = form.cleaned_data['next_schedule']
                Schedule.objects.create(patient=self.patient,
                                        subject=subj, schedule_date=schd_date)
            else:
                lastfollowup.visited = True
                lastfollowup.schedule_date = timezone.now()
                lastfollowup.save()

        self.slug = form.save().slug
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('sfsapp:activitydetail',
                       kwargs={'slug': self.slug})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        try:
            patient = Patient.objects.get(slug=self.kwargs['patient_slug'])
            context['thispatient'] = patient
            if Schedule.objects.filter(
                    patient=patient, schedule_date__gte=timezone.now()).exists():
                lastfollowup = Schedule.objects.get(
                    patient=patient, schedule_date__gte=timezone.now())
                context['lastfollowup'] = lastfollowup
        except Exception as e:
            print(e)
        return context


class ActivityUpdateView(ExpiryMixin,
                         SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'activitycreate.html'
    model = Activity
    form_class = ActivityForm
    success_url = reverse_lazy('sfsapp:home')
    success_message = "Visit (Activity) updated successfully."

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        activity = Activity.objects.get(slug=self.kwargs['slug'])
        self.patient = activity.patient
        if form.cleaned_data['doctor']:
            # print("from cleaned data")
            form.instance.doctor = form.cleaned_data['doctor']
        else:
            # print("from patient")
            form.instance.doctor = self.patient.doctor
        # last followup
        followup_exists = Schedule.objects.filter(
            patient=self.patient,
            schedule_date__gte=timezone.now()).exists()
        assign_schedule_clicked = form.cleaned_data['assign_schedule']
        try:
            notlast = self.kwargs['notlast']
            last = False
        except Exception as e:
            notlast = False
            last = True
        if followup_exists is False and assign_schedule_clicked and last:

            # print("followup doesnot exist and clicked, new schedule created")
            subject = form.cleaned_data['subject']
            date = form.cleaned_data['next_schedule']
            Schedule.objects.create(
                patient=self.patient, subject=subject, schedule_date=date)
        elif followup_exists and last:
            lastfollowup = Schedule.objects.get(
                patient=self.patient, schedule_date__gte=timezone.now())
            if assign_schedule_clicked:
                # print("followup exists and clicked, schedule updated")
                lastfollowup.subject = form.cleaned_data['subject']
                lastfollowup.schedule_date = form.cleaned_data['next_schedule']
                lastfollowup.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('sfsapp:activitydetail',
                       kwargs={'slug': self.kwargs['slug']})

    def get_context_data(self, **kwargs):
        activity = Activity.objects.get(slug=self.kwargs['slug'])
        self.patient = activity.patient
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        if Schedule.objects.filter(patient=self.patient,
                                   schedule_date__gte=timezone.now()).exists():
            lastfollowup = Schedule.objects.get(
                patient=self.patient, schedule_date__gte=timezone.now())
            context['lastfollowup'] = lastfollowup
        return context


class ActivityDeleteView(SuccessMessageMixin, AdminRequiredMixin, DeleteView):
    template_name = 'activitydelete.html'
    model = Activity
    success_url = reverse_lazy('sfsapp:activitylist')
    success_message = "Visit (Activity) deleted successfully."

    def get_success_url(self):
        activity = Activity.objects.get(slug=self.kwargs['slug'])
        patient_slug = activity.patient.slug
        return reverse('sfsapp:patientdetail', kwargs={'slug': patient_slug})


# Transaction Views

class GetPreviousDueView(ExpiryMixin, LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'transactions/getpreviousdue.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ordernowform'] = OrderForm
        if self.request.GET:
            patient_id = self.request.GET.get('patient_id')
            patient = Patient.objects.get(id=patient_id)
            previous_transaction = Transaction.objects.filter(
                patient=patient).order_by('id')
            if previous_transaction.exists():
                context['previous_due'] = previous_transaction.last().due
            else:
                context['previous_due'] = 0
        return context


class TransactionListView(ExpiryMixin, LoginRequiredMixin, ListView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'transactions/transactionlist.html'
    queryset = Transaction.objects.all().order_by('-id')
    context_object_name = 'transactionlist'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['patientlist'] = Patient.objects.all()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        total_amount = 0
        paid_amount = 0
        due_amount = 0
        for patient in Patient.objects.all():
            total_amount = patient.total_amount + total_amount
            paid_amount = patient.paid_amount + paid_amount
            due_amount = patient.due_amount + due_amount
        context['total_amount'] = total_amount
        context['paid_amount'] = paid_amount
        context['due_amount'] = due_amount
        return context


class TransactionCreateView(ExpiryMixin, SuccessMessageMixin, LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'transactions/transactioncreate.html'
    form_class = TransactionForm
    success_url = reverse_lazy('sfsapp:transactionlist')
    success_message = "Transaction Created Successfully"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        try:
            context['thispatientdue'] = Patient.objects.get(
                slug=self.kwargs['slug']).due_amount
            context['thispatient_id'] = Patient.objects.get(
                slug=self.kwargs['slug']).id
        except:
            pass
        return context

    def form_valid(self, form):
        form.instance.received_by = self.request.user
        try:
            self.patient = Patient.objects.get(
                slug=self.kwargs['slug'])
            form.instance.patient = self.patient

        except Exception as e:
            self.patient = form.cleaned_data['patient']
            form.instance.patient = self.patient
        form.save()
        patient = form.instance.patient
        patient.total_amount = patient.total_amount + form.instance.net
        patient.paid_amount = patient.paid_amount + form.instance.advance
        patient.due_amount = patient.due_amount + \
            form.instance.due - form.instance.previous_due
        patient.save()
        return super().form_valid(form)


class TransactionDetailView(ExpiryMixin, LoginRequiredMixin, DetailView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'transactions/transactiondetail.html'
    model = Transaction
    context_object_name = 'transactiondetail'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        return context


class PatientTransactionListView(ExpiryMixin, LoginRequiredMixin, DetailView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'transactions/patienttransactionlist.html'
    model = Patient
    context_object_name = 'patientdetail'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        return context


#Account Summary
class AccountSummaryView(ExpiryMixin, LoginRequiredMixin, ListView):
    login_url = reverse_lazy('sfsapp:login')
    template_name = 'transactions/accountsummary.html'
    queryset = Patient.objects.filter(due_amount__gt=0).order_by('-due_amount')
    context_object_name = 'patientswithdue'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = Organization.objects.all().first()
        context['ordernowform'] = OrderForm
        context['organization'] = organization
        return context
